# mlpytorchtxtgen

kaggle datasets create -p data/
kaggle datasets version -p data/ -m "Updated data fin"


kaggle kernels push -p train/
kaggle kernels status sipvip/mlpytorchtxtgen
kaggle kernels output sipvip/mlpytorchtxtgen -p output/
cp output/model0.pt data/model0.pt
cd data
tar cfv libs.tar libs/
cd -
kaggle datasets version -r tar -p data/ -m "Updated data poet4"
kaggle datasets status sipvip/torchpoetdata


kaggle kernels push -p dataloader/
kaggle kernels status sipvip/mlpytorchtxtdataloader


kaggle kernels push -p generator/
kaggle kernels status sipvip/torchpoetgen
kaggle kernels output sipvip/torchpoetgen -p output/