c2i = {}
i2c = {}
data = ''

f = open('../input/finbot.txt')

data = f.read()

ind = 0
for c in data:  # sets are not deterministic
    if c not in c2i:
        c2i[c] = ind
        i2c[ind] = c
        ind += 1

vocab_size = len(i2c)
ptr = 0


def get_slice(ptr, length):
    if (ptr + length >= len(data)):
        ptr = 0
    if ptr == 0:
        rewind = True
    else:
        rewind = False
    sl = data[ptr:ptr+length]
    ptr += length
    return sl, rewind

def get_sample(ptr, length):
    s, rewind = get_slice(ptr, length+1)
    X = [c2i[c] for c in s[:-1]]
    y = [c2i[c] for c in s[1:]]
    return (X, y, rewind)


sl, rewind = get_slice(ptr, vocab_size)

print(sl,rewind)
for x in range(6):
    sl, rewind = get_slice(x, vocab_size)
    print(sl)

# if rewind:
#     print(rewind)
#     sl, rewind = get_slice(ptr, vocab_size)
#     print(sl)

