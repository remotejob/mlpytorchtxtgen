import pickle
import numpy as np
from torch import Tensor
import torch.nn as nn
import torch

force_cpu = False

if torch.cuda.is_available() and force_cpu is not True:
    device = 'cuda'
    use_cuda = True
    print("Running on GPU")
else:
    device = 'cpu'
    use_cuda = False
    print("Running on CPU")
    print("Note: on Google Colab, make sure to select:")
    print("      Runtime / Change Runtime Type / Hardware accelerator: GPU")


c2i = {}
i2c = {}
data = ''

f = open('../input/finbot.txt')

data = f.read()

ind = 0
for c in data:  # sets are not deterministic
    if c not in c2i:
        c2i[c] = ind
        i2c[ind] = c
        ind += 1

# print(c2i)
# print(i2c)


with open('c2i.pickle', 'wb') as handle:
    pickle.dump(c2i, handle, protocol=pickle.HIGHEST_PROTOCOL)

with open('i2c.pickle', 'wb') as handle:
    pickle.dump(i2c, handle, protocol=pickle.HIGHEST_PROTOCOL)


with open('c2i.pickle', 'rb') as handle:
    dc2i = pickle.load(handle)

with open('i2c.pickle', 'rb') as handle:
    di2c = pickle.load(handle)

print(dc2i)
print(di2c)


ptr = 0


def get_slice(ptr, length):
    if (ptr + length >= len(data)):
        ptr = 0
    if ptr == 0:
        rewind = True
    else:
        rewind = False
    sl = data[ptr:ptr+length]
    ptr += length
    return sl, rewind


def get_sample(ptr, length):
    s, rewind = get_slice(ptr, length+1)
    X = np.array([c2i[c] for c in s[:-1]], dtype=int)
    y = np.array([c2i[c] for c in s[1:]], dtype=int)
    return (X, y, rewind)


vocab_size = len(di2c)


def get_sample_batch(batch_size, length):
    smpX = np.zeros((batch_size, length), dtype=int)
    smpy = np.zeros((batch_size, length), dtype=int)
    for i in range(batch_size):
        smpX[i, :], smpy[i, :], _ = get_sample(i, length)
    return smpX, smpy


# smpX, smpy = get_sample_batch(3, vocab_size)

# print(smpX)
# print(smpy)


def one_hot(p, dim):
    o = np.zeros(p.shape+(dim,), dtype=int)
    for y in range(p.shape[0]):
        for x in range(p.shape[1]):
            o[y, x, p[y, x]] = 1
    return o


def get_data():
    X, y = get_sample_batch(15000, vocab_size)
    Xo = one_hot(X, vocab_size)

    # Xt = Tensor(torch.from_numpy(np.array(Xo,dtype=np.float32)), requires_grad=False, dtype=torch.float32, device=device)
    # yt = Tensor(torch.from_numpy(y), requires_grad=False, dtype=torch.int32, device=device)
    Xt = Tensor(torch.from_numpy(np.array(Xo, dtype=np.float32))).to(device)
    Xt.requires_grad_(False)
    yt = torch.LongTensor(torch.from_numpy(
        np.array(y, dtype=np.int64))).to(device)
    yt.requires_grad_(False)
    return Xt, yt


Xt, yt = get_data()

# print(Xt)
# print(yt)


class Poet(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers, output_size, device):
        super(Poet, self).__init__()

        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.output_size = output_size
        self.device = device

        self.lstm = nn.LSTM(input_size=input_size, hidden_size=hidden_size,
                            num_layers=num_layers, batch_first=True, dropout=0)

        self.demb = nn.Linear(hidden_size, output_size)
        # negative dims are a recent thing (as 2018-03), remove for old vers.
        self.softmax = nn.Softmax(dim=-1)

    def init_hidden(self, batch_size):
        self.h0 = torch.zeros(self.num_layers, batch_size,
                              self.hidden_size, device=self.device)
        self.c0 = torch.zeros(self.num_layers, batch_size,
                              self.hidden_size, device=self.device)

    def forward(self, inputx, steps):
        self.lstm.flatten_parameters()
        hn, (self.h0, self.c0) = self.lstm(
            inputx.to(self.device), (self.h0, self.c0))
        hnr = hn.contiguous().view(-1, self.hidden_size)
        op = self.demb(hnr)
        opr = op.view(-1, steps, self.output_size)
        return opr


model_finbot_params = {
    "model_name": "lib",
    "vocab_size": vocab_size,
    "neurons": 256,
    "layers": 2,
    "learning_rate": 1.e-3,
    "steps": 10,  # was 80
    "batch_size": 256  # was 128
}

model_params = model_finbot_params
steps = model_params['steps']


print("fresh start")
poet = Poet(vocab_size, model_params['neurons'],
            model_params['layers'], vocab_size, device).to(device)
opti = torch.optim.Adam(
    poet.parameters(), lr=model_params['learning_rate'])


criterion = nn.CrossEntropyLoss()


def train(Xt, yt, bPr=False):
    poet.zero_grad()

    poet.init_hidden(Xt.size(0))
    output = poet(Xt, steps)

    olin = output.view(-1, vocab_size)
    _, ytp = torch.max(olin, 1)
    ytlin = yt.view(-1)

    pr = 0.0
    if bPr:  # Calculate precision
        ok = 0
        nok = 0
        for i in range(ytlin.size()[0]):
            i1 = ytlin[i].item()
            i2 = ytp[i].item()
            if i1 == i2:
                ok = ok + 1
            else:
                nok = nok+1
            pr = ok/(ok+nok)

    loss = criterion(olin, ytlin)
    ls = loss.item()
    loss.backward()
    opti.step()

    return ls, pr


ls, pr = train(Xt, yt, bPr=True)

print(ls)
print(pr)
# import sys
# sys.path.insert(0, "../input/libs")
# from textlibrary import TextLibrary
# from poet import Poet
# from torch import Tensor
# import torch.nn as nn
# import torch
# import random
# import json
# import os
# import numpy as np


# libdesc = {
#     "name": "TinyFinbot",
#     "description": "twiiter dialogs",
#     "lib": '../input/finbot.txt'

# }

# force_cpu = False

# if torch.cuda.is_available() and force_cpu is not True:
#     device = 'cuda'
#     use_cuda = True
#     print("Running on GPU")
# else:
#     device = 'cpu'
#     use_cuda = False
#     print("Running on CPU")
#     print("Note: on Google Colab, make sure to select:")
#     print("      Runtime / Change Runtime Type / Hardware accelerator: GPU")


# textlib = TextLibrary(libdesc["lib"])

# model_finbot_params = {
#     "model_name": "lib",
#     "vocab_size": len(textlib.i2c),
#     "neurons": 256,
#     "layers": 2,
#     "learning_rate": 1.e-3,
#     "steps": 80,
#     "batch_size": 256 #was 128
# }

# model_params = model_finbot_params
# batch_size = model_params['batch_size']
# vocab_size = model_params['vocab_size']
# steps = model_params['steps']


# def one_hot(p, dim):
#     o = np.zeros(p.shape+(dim,), dtype=int)
#     for y in range(p.shape[0]):
#         for x in range(p.shape[1]):
#             o[y, x, p[y, x]] = 1
#     return o


# def get_data():
#     X, y = textlib.get_random_sample_batch(batch_size, steps)
#     Xo = one_hot(X, vocab_size)

#     # Xt = Tensor(torch.from_numpy(np.array(Xo,dtype=np.float32)), requires_grad=False, dtype=torch.float32, device=device)
#     # yt = Tensor(torch.from_numpy(y), requires_grad=False, dtype=torch.int32, device=device)
#     Xt = Tensor(torch.from_numpy(np.array(Xo, dtype=np.float32))).to(device)
#     Xt.requires_grad_(False)
#     yt = torch.LongTensor(torch.from_numpy(
#         np.array(y, dtype=np.int64))).to(device)
#     yt.requires_grad_(False)
#     return Xt, yt


# def show_gpu_mem(context="all"):
#     if use_cuda:
#         print("[{}] Memory allocated: {} max_alloc: {} cached: {} max_cached: {}".format(context, torch.cuda.memory_allocated(
#         ), torch.cuda.max_memory_allocated(), torch.cuda.memory_cached(), torch.cuda.max_memory_cached()))


# """## Create a poet"""
# exists = os.path.isfile('../input/model0.pt')
# if exists:
#     # Store configuration file values
#     print("model exist!!!")
#     checkpoint = torch.load("../input/model0.pt")
#     poet = Poet(vocab_size, model_params['neurons'],
#                 model_params['layers'], vocab_size, device).to(device)
#     poet.load_state_dict(checkpoint['state_dict'])
#     opti = torch.optim.Adam(
#         poet.parameters(), lr=model_params['learning_rate'])
#     opti.load_state_dict(checkpoint['optimizer'])

# else:
#     print("fresh start")
#     poet = Poet(vocab_size, model_params['neurons'],
#                 model_params['layers'], vocab_size, device).to(device)
#     opti = torch.optim.Adam(
#         poet.parameters(), lr=model_params['learning_rate'])

# """## Training helpers"""

# criterion = nn.CrossEntropyLoss()

# bok = 0


# def train(Xt, yt, bPr=False):
#     poet.zero_grad()

#     poet.init_hidden(Xt.size(0))
#     output = poet(Xt, steps)

#     olin = output.view(-1, vocab_size)
#     _, ytp = torch.max(olin, 1)
#     ytlin = yt.view(-1)

#     pr = 0.0
#     if bPr:  # Calculate precision
#         ok = 0
#         nok = 0
#         for i in range(ytlin.size()[0]):
#             i1 = ytlin[i].item()
#             i2 = ytp[i].item()
#             if i1 == i2:
#                 ok = ok + 1
#             else:
#                 nok = nok+1
#             pr = ok/(ok+nok)

#     loss = criterion(olin, ytlin)
#     ls = loss.item()
#     loss.backward()
#     opti.step()

#     return ls, pr


# """## The actual training"""

# ls = 0
# nrls = 0
# if use_cuda:
#     intv = 500
# else:
#     intv = 10
# for e in range(1100000):  # 1100000 OK 6.3 hours
#     Xt, yt = get_data()
#     if (e+1) % intv == 0:
#         l, pr = train(Xt, yt, True)
#     else:
#         l, pr = train(Xt, yt, False)
#     ls = ls+l
#     nrls = nrls+1
#     if (e+1) % intv == 0:
#         print("Loss: {} Precision: {}".format(ls/nrls, pr))
#         # if use_cuda:
#         #     print("Memory allocated: {} max_alloc: {} cached: {} max_cached: {}".format(torch.cuda.memory_allocated(
#         #     ), torch.cuda.max_memory_allocated(), torch.cuda.memory_cached(), torch.cuda.max_memory_cached()))
#         nrls = 0
#         ls = 0

# print("Memory allocated: {} max_alloc: {} cached: {} max_cached: {}".format(torch.cuda.memory_allocated(
# ), torch.cuda.max_memory_allocated(), torch.cuda.memory_cached(), torch.cuda.max_memory_cached()))

# # def detectPlagiarism(generatedtext, textlibrary, minQuoteLength=10):
# #     textlibrary.source_highlight(generatedtext, minQuoteLength)

# # detectPlagiarism(tgen, textlib)


# def save_checkpoint(state, filename='model0.pt'):
#     torch.save(state, filename)
#     # if is_best:
#     #     shutil.copyfile(filename, 'model_best.pth')


# # best_prec1 = 64.4

# save_checkpoint({
#     'epoch': e,
#     'arch': "poet8",
#             'state_dict': poet.state_dict(),
#             # 'best_prec1': best_prec1,
#             'optimizer': opti.state_dict(),
#             'i2c': textlib.i2c,
#             'c2i': textlib.c2i,
# })
